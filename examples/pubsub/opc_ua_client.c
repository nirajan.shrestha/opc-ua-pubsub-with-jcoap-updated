#include <signal.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <time.h>
#include "open62541.h"

#define NUM_VARIABLES 	14

UA_UInt32 variableId[NUM_VARIABLES] = {1020, 1021, 1022, 1023, 1024, 1025, 1026, 1027, 1028, 1029, 1030, 1031, 1032, 1033};

UA_Boolean loop = true;
static void stopHandler(int sign) {
    UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "received ctrl-c");
    loop = false;
}

UA_StatusCode writeValue(UA_Client *client, UA_Variant value, UA_Int32 objectId, UA_Int32 variableValue) {
	UA_NodeId nodeId= UA_NODEID_NUMERIC(0, objectId);
	//UA_VariableAttributes attr = UA_VariableAttributes_default;
	UA_Variant_setScalarCopy(&value, &variableValue, &UA_TYPES[UA_TYPES_INT32]); 
	//return UA_Server_writeValue(client, nodeId, value);
    return UA_Client_writeValueAttribute(client, nodeId, &value);
	//return UA_Client_writeValueAttribute(client, nodeId, &attr.value);
}

void getValue(UA_Client *client, UA_Variant value) {
	UA_Int32 objectId;
    UA_Int32 variableId;
    UA_Int32 variableValue;

	printf("Please choose object (bdc-1, cdc-2, hc-3, ps-4, ws-5, ss-6): ");
	scanf("%d", &objectId);
	printf("Please choose variable (stt-1, spd-2, psx-11, psy-12, psz-13, wgt-3, lrm-4): ");
	scanf("%d", &variableId);
	printf("Please enter value (1, 0, -1): ");
	scanf("%d", &variableValue);

	switch(objectId) {
		case 1:	// BridgeDrvieController
			if(variableId==1) {
				writeValue(client, value, 1021, variableValue);
				UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "CLIENT WRITE: BridgeDrvieController, State, nodeId=1021, value=%u\n", variableValue);
			}
			else if(variableId==2) {
				writeValue(client, value, 1020, variableValue);
				UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "CLIENT WRITE: BridgeDrvieController, Speed, nodeId=1020, value=%u\n", variableValue);
			}
			break;
		case 2:	// CrabDrvieController
			if(variableId==1) {
				writeValue(client, value, 1023, variableValue);
				UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "CLIENT WRITE: CrabDrvieController, State, nodeId=1023, value=%u\n", variableValue);
			}
			else if(variableId==2) {
				writeValue(client, value, 1022, variableValue);
				UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "CLIENT WRITE: CrabDrvieController, Speed, nodeId=1022, value=%u\n", variableValue);
			}
			break;
		case 3:	// HoistController
			if(variableId==1) {
				writeValue(client, value, 1025, variableValue);
				UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "CLIENT WRITE: HoistController, State, nodeId=1025, value=%u\n", variableValue);
			}
			else if(variableId==2) {
				writeValue(client, value, 1024, variableValue);
				UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "CLIENT WRITE: HoistController, Speed, nodeId=1024, value=%u\n", variableValue);
			}
			break;
		case 4:	// PositionSensor
			if(variableId==1) {
				writeValue(client, value, 1029, variableValue);
				UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "CLIENT WRITE: PositionSensor, State, nodeId=1029, value=%u\n", variableValue);
			}
			else if(variableId==11) {
				writeValue(client, value, 1026, variableValue);
				UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "CLIENT WRITE: PositionSensor, PosX, nodeId=1026, value=%u\n", variableValue);
			}
			else if(variableId==12) {
				writeValue(client, value, 1027, variableValue);
				UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "CLIENT WRITE: PositionSensor, PosY, nodeId=1027, value=%u\n", variableValue);
			}
			else if(variableId==13) {
				writeValue(client, value, 1028, variableValue);
				UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "CLIENT WRITE: PositionSensor, PosZ, nodeId=1028, value=%u\n", variableValue);
			}
			break;
		case 5: // WeightSensor
			if(variableId==1) {
				writeValue(client, value, 1031, variableValue);
				UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "CLIENT WRITE: WeightSensor, State, nodeId=1031, value=%u\n", variableValue);
			}
			else if(variableId==3) {
				writeValue(client, value, 1030, variableValue);
				UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "CLIENT WRITE: WeightSensor, Weight, nodeId=1030, value=%u\n", variableValue);
			}
			break;
		case 6: // SwingSensor
			if(variableId==1) {
				writeValue(client, value, 1033, variableValue);
				UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "CLIENT WRITE: SwingSensor, State, nodeId=1033, value=%u\n", variableValue);
			}
			else if(variableId==4) {
				writeValue(client, value, 1032, variableValue);
				UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "CLIENT WRITE: SwingSensor, Alarm, nodeId=1032, value=%u\n", variableValue);
			}
			break;
	}
}

void generateValue(UA_Client *client, UA_Variant value, UA_Int32 variableId) {
	UA_Int32 objectId;
	UA_Int32 variableValue;

	variableValue = rand() % 10;

	UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "CLIENT WRITE: nodeId=%u, value=%u\n", variableId, variableValue);
	writeValue(client, value, variableId, variableValue);
}

int main(void) {
    signal(SIGINT, stopHandler);
    signal(SIGTERM, stopHandler);

    UA_Client *client = UA_Client_new(UA_ClientConfig_default);
    UA_StatusCode retval = UA_Client_connect(client, "opc.tcp://localhost:4840");
    if(retval != UA_STATUSCODE_GOOD) {
        UA_Client_delete(client);
        return (int)retval;
    }

    srand(time(NULL));

    UA_Variant value; /* Variants can hold scalar values and arrays of any type */
    UA_Variant_init(&value);
    int i=0;

    while(loop)	{
//    	getValue(client, value);
    	generateValue(client, value, variableId[i]);
    	sleep(3);
    	i++;
		if(i>=NUM_VARIABLES)
			i=0;
    }

    /* Clean up */
    UA_Variant_deleteMembers(&value);
    UA_Client_delete(client); /* Disconnects the client internally */
    return UA_STATUSCODE_GOOD;
}
