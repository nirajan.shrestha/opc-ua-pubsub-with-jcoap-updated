  
#include <stdio.h>
#include <stdlib.h>
#include <memory.h>
#include <stdio.h>
#include <signal.h>

#include <sys/time.h>

#include "ua_pubsub_networkmessage.h"
#include "ua_log_stdout.h"
#include "ua_server.h"
#include "ua_config_default.h"
#include "ua_pubsub.h"
//#include "ua_network_pubsub_mqtt.h"
#include "ua_network_pubsub_coap.h"
#include "src_generated/ua_types_generated.h"

//#include "ua_client.h"
//#include "ua_client_subscriptions.h"

#include <signal.h>
//#include "../../plugins/mqtt/ua_mqtt_adapter.h"

UA_NodeId connectionIdent, publishedDataSetIdent, writerGroupIdent;

UA_NetworkAddressUrlDataType networkAddressUrl;

static void callback(){
     UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "Yeah Message Received successfully !!");
     
     /* For example try to decode as a Json Networkmessage...
     UA_NetworkMessage dst;
     UA_StatusCode ret = UA_NetworkMessage_decodeJson(&dst, encodedBuffer);
     if( ret == UA_STATUSCODE_GOOD){
     }
      */
     
     
     //UA_NetworkMessage_deleteMembers(&dst);
}

static void
addPubSubConnection(UA_Server *server){

	//UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "inside addPubSubConnection()\n");
    UA_PubSubConnectionConfig connectionConfig;
    memset(&connectionConfig, 0, sizeof(connectionConfig));
    connectionConfig.name = UA_STRING("OPC UA Initialization");
    connectionConfig.transportProfileUri =
    UA_STRING("http://opcfoundation.org/UA-Profile/Transport/pubsub-udp-uadp");
    connectionConfig.enabled = UA_TRUE;    
    UA_Variant_setScalar(&connectionConfig.address, &networkAddressUrl,
                         &UA_TYPES[UA_TYPES_NETWORKADDRESSURLDATATYPE]);
    connectionConfig.publisherId.numeric = UA_UInt32_random();
    
    /*UA_KeyValuePair connectionOptions[1];
    connectionOptions[0].key = UA_QUALIFIEDNAME(0, "helloTopic");
    UA_String mqttClientId = UA_STRING(".well-known/core");
    UA_Variant_setScalar(&connectionOptions[0].value, &mqttClientId, &UA_TYPES[UA_TYPES_STRING]);
    
    connectionConfig.connectionProperties = connectionOptions;
    connectionConfig.connectionPropertiesSize = 1;*/

    UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "before calling UA_Server_addPubSubConnection()\n");
    //printf("BEFORE connectionIdent %s\n",connectionIdent.identifier.string.data);
    UA_Server_addPubSubConnection(server, &connectionConfig, &connectionIdent); // this is where the connection an
	//printf("AFTER connectionIdent %s\n",connectionIdent.identifier.string.data);
	UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "after calling UA_Server_addPubSubConnection()\n");
}

static void
addRead(UA_Server *server, UA_PubSubConnection *connection){
   //Register Transport settings
    UA_BrokerWriterGroupTransportDataType brokerTransportSettings;
    memset(&brokerTransportSettings, 0, sizeof(UA_BrokerWriterGroupTransportDataType));
    brokerTransportSettings.queueName = UA_STRING(".well-known/core");
    brokerTransportSettings.resourceUri = UA_STRING_NULL;
    brokerTransportSettings.authenticationProfileUri = UA_STRING_NULL;

    UA_ExtensionObject transportSettings;
    memset(&transportSettings, 0, sizeof(UA_ExtensionObject));
    transportSettings.encoding = UA_EXTENSIONOBJECT_DECODED;
    transportSettings.content.decoded.type = &UA_TYPES[UA_TYPES_BROKERWRITERGROUPTRANSPORTDATATYPE];
    transportSettings.content.decoded.data = &brokerTransportSettings;

	UA_PubSubChannelDataCoAP * channelData =
            (UA_PubSubChannelDataCoAP *) UA_calloc(1, (sizeof(UA_PubSubChannelDataCoAP)));
	channelData->callback = &callback;
	connection->channel->handle = channelData;

    UA_StatusCode rv = connection->channel->receive(connection->channel, NULL, &transportSettings, NULL);
}

static void
addPublishedDataSet(UA_Server *server) {
    /* The PublishedDataSetConfig contains all necessary public
    * informations for the creation of a new PublishedDataSet */
    UA_PublishedDataSetConfig publishedDataSetConfig;
    memset(&publishedDataSetConfig, 0, sizeof(UA_PublishedDataSetConfig));
    publishedDataSetConfig.publishedDataSetType = UA_PUBSUB_DATASET_PUBLISHEDITEMS;
    publishedDataSetConfig.name = UA_STRING("Demo PDS");
    /* Create new PublishedDataSet based on the PublishedDataSetConfig. */
    UA_Server_addPublishedDataSet(server, &publishedDataSetConfig, &publishedDataSetIdent);
}

static void
addVariable(UA_Server *server) {
    // Define the attribute of the myInteger variable node 
    UA_VariableAttributes attr = UA_VariableAttributes_default;
    UA_Int32 myInteger = 42;
    UA_Variant_setScalar(&attr.value, &myInteger, &UA_TYPES[UA_TYPES_INT32]);
    attr.description = UA_LOCALIZEDTEXT("en-US","the answer");
    attr.displayName = UA_LOCALIZEDTEXT("en-US","the answer");
    attr.dataType = UA_TYPES[UA_TYPES_INT32].typeId;
    attr.accessLevel = UA_ACCESSLEVELMASK_READ | UA_ACCESSLEVELMASK_WRITE;

    // Add the variable node to the information model 
    UA_NodeId myIntegerNodeId = UA_NODEID_NUMERIC(1, 42);
    UA_QualifiedName myIntegerName = UA_QUALIFIEDNAME(1, "the answer");
    UA_NodeId parentNodeId = UA_NODEID_NUMERIC(0, UA_NS0ID_OBJECTSFOLDER);
    UA_NodeId parentReferenceNodeId = UA_NODEID_NUMERIC(0, UA_NS0ID_ORGANIZES);
    UA_Server_addVariableNode(server, myIntegerNodeId, parentNodeId,
                              parentReferenceNodeId, myIntegerName,
                              UA_NODEID_NUMERIC(0, UA_NS0ID_BASEDATAVARIABLETYPE), attr, NULL, NULL);
}

/**
 * **DataSetField handling**
 * The DataSetField (DSF) is part of the PDS and describes exactly one published field.
 */
static void
addDataSetField(UA_Server *server) {
    /* Add a field to the previous created PublishedDataSet */
    
    UA_NodeId dataSetFieldIdent;
    UA_DataSetFieldConfig dataSetFieldConfig;
    memset(&dataSetFieldConfig, 0, sizeof(UA_DataSetFieldConfig));
    dataSetFieldConfig.dataSetFieldType = UA_PUBSUB_DATASETFIELD_VARIABLE;

    dataSetFieldConfig.field.variable.fieldNameAlias = UA_STRING("X-axis");
    dataSetFieldConfig.field.variable.promotedField = UA_FALSE;
    dataSetFieldConfig.field.variable.publishParameters.publishedVariable = UA_NODEID_NUMERIC(1, 42);
    dataSetFieldConfig.field.variable.publishParameters.attributeId = UA_ATTRIBUTEID_VALUE;
    UA_Server_addDataSetField(server, publishedDataSetIdent, &dataSetFieldConfig, &dataSetFieldIdent);
}

/**
 * **WriterGroup handling**
 * The WriterGroup (WG) is part of the connection and contains the primary configuration
 * parameters for the message creation.
 */
static void
addWriterGroup(UA_Server *server, UA_PubSubConnection *connection) {
    /* Now we create a new WriterGroupConfig and add the group to the existing PubSubConnection. */
    UA_WriterGroupConfig writerGroupConfig;
    memset(&writerGroupConfig, 0, sizeof(UA_WriterGroupConfig));
    writerGroupConfig.name = UA_STRING("Demo WriterGroup");
    writerGroupConfig.publishingInterval = 500;
    writerGroupConfig.enabled = UA_FALSE;
    writerGroupConfig.writerGroupId = 100;
    
    /* Choose the encoding, UA_PUBSUB_ENCODING_JSON is available soon */
    writerGroupConfig.encodingMimeType = UA_PUBSUB_ENCODING_JSON; 
    
    UA_BrokerWriterGroupTransportDataType brokerTransportSettings;
    memset(&brokerTransportSettings, 0, sizeof(UA_BrokerWriterGroupTransportDataType));
    brokerTransportSettings.queueName = UA_STRING("hello");
    brokerTransportSettings.resourceUri = UA_STRING_NULL;
    brokerTransportSettings.authenticationProfileUri = UA_STRING_NULL;
    
    UA_ExtensionObject transportSettings;
    memset(&transportSettings, 0, sizeof(UA_ExtensionObject));
    transportSettings.encoding = UA_EXTENSIONOBJECT_DECODED;
    transportSettings.content.decoded.type = &UA_TYPES[UA_TYPES_BROKERWRITERGROUPTRANSPORTDATATYPE];
    transportSettings.content.decoded.data = &brokerTransportSettings;
    
    writerGroupConfig.transportSettings = transportSettings;
    /* The configuration flags for the messages are encapsulated inside the
     * message- and transport settings extension objects. These extension objects
     * are defined by the standard. e.g. UadpWriterGroupMessageDataType */

	UA_PubSubChannelDataCoAP * channelData =
            (UA_PubSubChannelDataCoAP *) UA_calloc(1, (sizeof(UA_PubSubChannelDataCoAP)));
	channelData->callback = &callback;
	connection->channel->handle = channelData;
	connection->channel->state = UA_PUBSUB_CHANNEL_PUB_SUB;

    
	UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "before calling UA_Server_addWriterGroup()\n");
    UA_Server_addWriterGroup(server, connectionIdent, &writerGroupConfig, &writerGroupIdent);
	//UA_StatusCode rv = connection->channel->send(connection->channel, &transportSettings, NULL);
	UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "after calling UA_Server_addWriterGroup()\n");
}

/**
 * **DataSetWriter handling**
 * A DataSetWriter (DSW) is the glue between the WG and the PDS. The DSW is linked to exactly one
 * PDS and contains additional informations for the message generation.
 */
static void
addDataSetWriter(UA_Server *server) {
    /* We need now a DataSetWriter within the WriterGroup. This means we must
     * create a new DataSetWriterConfig and add call the addWriterGroup function. */
    UA_NodeId dataSetWriterIdent;
    UA_DataSetWriterConfig dataSetWriterConfig;
    memset(&dataSetWriterConfig, 0, sizeof(UA_DataSetWriterConfig));
    dataSetWriterConfig.name = UA_STRING("Demo DataSetWriter");
    dataSetWriterConfig.dataSetWriterId = 62541;
    dataSetWriterConfig.keyFrameCount = 10;
    
    /* JSON config for the dataSetWriter */
    UA_JsonDataSetWriterMessageDataType jsonDswMd;
    jsonDswMd.dataSetMessageContentMask = (UA_JsonDataSetMessageContentMask)
            (UA_JSONDATASETMESSAGECONTENTMASK_DATASETWRITERID 
            | UA_JSONDATASETMESSAGECONTENTMASK_SEQUENCENUMBER
            | UA_JSONDATASETMESSAGECONTENTMASK_STATUS
            | UA_JSONDATASETMESSAGECONTENTMASK_METADATAVERSION
            | UA_JSONDATASETMESSAGECONTENTMASK_TIMESTAMP);
    
    UA_ExtensionObject messageSettings;
    messageSettings.encoding = UA_EXTENSIONOBJECT_DECODED;
    messageSettings.content.decoded.type = &UA_TYPES[UA_TYPES_JSONDATASETWRITERMESSAGEDATATYPE];
    messageSettings.content.decoded.data = &jsonDswMd;
    
    dataSetWriterConfig.messageSettings = messageSettings;
    
    
    UA_Server_addDataSetWriter(server, writerGroupIdent, publishedDataSetIdent,
                               &dataSetWriterConfig, &dataSetWriterIdent);
}

/* Periodically refreshes the CoAP stack (sending/receiving) */
static void
CoAPYieldPollingCallback(UA_Server *server, UA_PubSubConnection *connection) {
	UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "before yield().\n");	
	printf("pollingcallback method called..\n");
    connection->channel->yield(connection->channel);
	UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "after yield().\n");
}

/* Adds a subscription */
static void 
addSubscription(UA_Server *server, UA_PubSubConnection *connection){
    
    //Register Transport settings
    UA_BrokerWriterGroupTransportDataType brokerTransportSettings;
    memset(&brokerTransportSettings, 0, sizeof(UA_BrokerWriterGroupTransportDataType));
    brokerTransportSettings.queueName = UA_STRING("hello");
    brokerTransportSettings.resourceUri = UA_STRING_NULL;
    brokerTransportSettings.authenticationProfileUri = UA_STRING_NULL;

    UA_ExtensionObject transportSettings;
    memset(&transportSettings, 0, sizeof(UA_ExtensionObject));
    transportSettings.encoding = UA_EXTENSIONOBJECT_DECODED;
    transportSettings.content.decoded.type = &UA_TYPES[UA_TYPES_BROKERWRITERGROUPTRANSPORTDATATYPE];
    transportSettings.content.decoded.data = &brokerTransportSettings;

    //channelData->callback = &callback;
    //connection->channel->handle = channelData;

    UA_StatusCode rv = connection->channel->regist(connection->channel, &transportSettings, &callback);
    if (rv == UA_STATUSCODE_GOOD) {
            UA_UInt64 subscriptionCallbackId;
            UA_Server_addRepeatedCallback(server, (UA_ServerCallback)CoAPYieldPollingCallback,
                                          connection, 200, &subscriptionCallbackId);
        } else {
            UA_LOG_WARNING(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "register channel failed: %s!",
                           UA_StatusCode_name(rv));
    }
    
    return; 
}

UA_Boolean running = true;
static void stopHandler(int sign) {
    UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "received ctrl-c");
    running = false;
}


int main(int argc, char **argv) {
    signal(SIGINT, stopHandler);
    signal(SIGTERM, stopHandler);

    networkAddressUrl.networkInterface = UA_STRING_NULL;
    networkAddressUrl.url = UA_STRING("opc.coap://127.0.0.1:5683/");
    if(argc > 1 && strncmp(argv[1], "opc.coap://", 11) == 0) {
        networkAddressUrl.url = UA_STRING(argv[1]);
    }

    UA_StatusCode retval = UA_STATUSCODE_GOOD;
    UA_ServerConfig *config = UA_ServerConfig_new_default();
    /* Details about the connection configuration and handling are located in
       the pubsub connection tutorial */
    config->pubsubTransportLayers = (UA_PubSubTransportLayer *) UA_malloc(1  * sizeof(UA_PubSubTransportLayer));
    if(!config->pubsubTransportLayers) {
        UA_ServerConfig_delete(config);
        return -1;
    }
	
    config->pubsubTransportLayers[0] = UA_PubSubTransportLayerCoAP();
    config->pubsubTransportLayersSize++;
    //printf("customHostname %s\n",(const char*)config->customHostname.data);
   UA_Server *server = UA_Server_new(config);

	

    addVariable(server);
    addPubSubConnection(server); 
    addPublishedDataSet(server);
    addDataSetField(server);
   

	//UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "before calling UA_PubSubConnection_findConnectionbyId()\n");
    UA_PubSubConnection *connection =
        UA_PubSubConnection_findConnectionbyId(server, connectionIdent);
	//UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "after calling UA_PubSubConnection_findConnectionbyId()\n");
	
    addWriterGroup(server, connection);
    addDataSetWriter(server);    

    if(connection != NULL) {
	//UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "before calling addRead()\n");
        addSubscription(server, connection);
	//addRead(server, connection);
	//UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "after calling addRead()\n");
    }

    //addClientSubscription(server);
    UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "before calling UA_Server_run()\n");
	//printf("customHostfname %s\n",(const char*)config->customHostname.data);
	
	
    retval |= UA_Server_run(server, &running);
    UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "after calling UA_Server_run()\n");
    UA_Server_delete(server);
    UA_ServerConfig_delete(config);
    return (int)retval;
}

