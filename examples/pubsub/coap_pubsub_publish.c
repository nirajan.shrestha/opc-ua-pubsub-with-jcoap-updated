  
#include <stdio.h>
#include <stdlib.h>
#include <memory.h>
#include <stdio.h>
#include <signal.h>

#include <sys/time.h>

#include "ua_pubsub_networkmessage.h"
#include "ua_log_stdout.h"
#include "ua_server.h"
#include "ua_config_default.h"
#include "ua_pubsub.h"
#include "ua_network_pubsub_coap.h"
#include "src_generated/ua_types_generated.h"

#include <signal.h>
#define NUM_OBJECT_TYPES 1
#define NUM_OBJECTS 	 7
#define NUM_VARIABLES 	 14

UA_NodeId connectionIdent, publishedDataSetIdent, writerGroupIdent;

UA_NetworkAddressUrlDataType networkAddressUrl;

// -----------------------------Beginning of crane model---------------------------------------------------

UA_Int32 myIntegerNodeId = 1;
UA_UInt16* n ;
/* Object type and its variable declaration */
UA_UInt32 nodeTypeId[NUM_OBJECT_TYPES]		={1000};
char *nodeTypeName[NUM_OBJECT_TYPES]		={"CraneType"};
UA_UInt32 nodeTypeVarId[NUM_OBJECT_TYPES]  	={1001};
char *nodeTypeVarName[NUM_OBJECT_TYPES] 	={"State"};

/* Object declaration */
UA_UInt32 nodeId[NUM_OBJECTS]				={1010, 1011, 1012, 1013, 1014, 1015, 1016};
char *nodeName[NUM_OBJECTS] 	 			={"Crane", "BridgeDriveController", "CrabDriveController", "HoistController", "PositionSensor", "WeightSensor", "SwingSensor"};
UA_UInt32 nodeParentId[NUM_OBJECTS]			={UA_NS0ID_OBJECTSFOLDER, 1010, 1010, 1010, 1010, 1010, 1010};
UA_UInt32 nodeReferenceTypeId[NUM_OBJECTS]	={UA_NS0ID_ORGANIZES, UA_NS0ID_HASCOMPONENT, UA_NS0ID_HASCOMPONENT, UA_NS0ID_HASCOMPONENT, UA_NS0ID_HASCOMPONENT, UA_NS0ID_HASCOMPONENT, UA_NS0ID_HASCOMPONENT};
char *nodeDescription[NUM_OBJECTS]			={"Crane", "BridgeDriveController", "CrabDriveController", "HoistController", "PositionSensor", "WeightSensor", "SwingSensor"};

/* Variable declaration within object*/
UA_UInt32 variableId[NUM_VARIABLES]			={1020, 1021, 1022, 1023, 1024, 1025, 1026, 1027, 1028, 1029, 1030, 1031, 1032, 1033};
char *variableName[NUM_VARIABLES] 			={"Speed", "State", "Speed", "State", "Speed", "State", "PositionX", "PositionY", "PositionZ", "State", "Weight", "State", "Alarm", "State"};
UA_UInt32 variableParentId[NUM_VARIABLES]	={1011, 1011, 1012, 1012, 1013, 1013, 1014, 1014, 1014, 1014, 1015, 1015, 1016, 1016};

/* DataType: Boolean-0, Double-1, Integer-2, String-3 */
UA_UInt32 variableDataType[NUM_VARIABLES]	={2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 1, 2, 2, 2};



/** afterWriteTime() function is called when variable value is updated  **/
static void afterWriteTime(UA_Server *server,
               const UA_NodeId *sessionId, void *sessionContext,
               const UA_NodeId *nodeId, void *nodeContext,
               const UA_NumericRange *range, const UA_DataValue *data) {
    UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "SERVER UPDATED: nodeId=%u, value=%d", nodeId->identifier, *(UA_UInt32 *)data->value.data);
	myIntegerNodeId = *(UA_UInt32 *) data->value.data;
	UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "assigned value=%d", myIntegerNodeId);


	/** read the value of a VariableNode  **/
	UA_UInt64 readValue_I32 = 0; // Set to a known value
	UA_Variant valueRead;
	UA_Variant_init(&valueRead); // Make sure everything is initialized and does not point to random locations
	UA_StatusCode  retvalRead = UA_Server_readValue(server, UA_NODEID_NUMERIC(n, 1020), &valueRead);
	if(retvalRead == UA_STATUSCODE_GOOD || UA_Variant_hasScalarType(&valueRead, &UA_TYPES[UA_TYPES_UINT32]))
	{
	  readValue_I32 = *(UA_UInt32*) valueRead.data;
	  UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "Checked value=%d", readValue_I32);
	}
	
}

static void addValueCallbackToCurrentTimeVariable(UA_Server *server) {
	UA_ValueCallback callback;
	UA_NodeId currentNodeId;
	callback.onWrite = afterWriteTime;
	for(int i=0; i<NUM_VARIABLES; i++)	{
		currentNodeId = UA_NODEID_NUMERIC(n, variableId[i]); //before it was 0 instead of n
		UA_Server_setVariableNode_valueCallback(server, currentNodeId, callback); // set up callback method if a value is changed for a vaiable node (variableID[i])
    }
}


static UA_StatusCode addObject(UA_Server *server, UA_UInt16* ns, UA_UInt32 requestedNewNodeId, UA_UInt32 parentNodeId, 
					UA_UInt32 referenceTypeId, char *browseName, char *description) {
	
	UA_StatusCode retVal = UA_STATUSCODE_GOOD;
	UA_ObjectAttributes attr = UA_ObjectAttributes_default;
	attr.displayName = UA_LOCALIZEDTEXT("en-US", browseName);
	attr.description = UA_LOCALIZEDTEXT("en-US", description);
	attr.writeMask = 0;
	attr.userWriteMask = 0;
	retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_OBJECT,
			UA_NODEID_NUMERIC(ns[0], requestedNewNodeId),
			UA_NODEID_NUMERIC(ns[0], parentNodeId),
			UA_NODEID_NUMERIC(ns[0], referenceTypeId),
			UA_QUALIFIEDNAME(ns[0], browseName),
			UA_NODEID_NUMERIC(ns[0], UA_NS0ID_BASEOBJECTTYPE),
			(const UA_NodeAttributes*)&attr,
			&UA_TYPES[UA_TYPES_OBJECTATTRIBUTES],
			NULL,
			NULL);

	return retVal;
}


// -----------------------------End of crane model---------------------------------------------------

static void
PollingCallback(UA_Server *server, UA_PubSubConnection *connection) {

    /*UA_NodeId myIntegerNodeId = UA_NODEID_NUMERIC(n, 1020);

   
    UA_Int32 myInteger = rand() % 10;
    UA_Variant myVar;
    UA_Variant_init(&myVar);
    UA_Variant_setScalar(&myVar, &myInteger, &UA_TYPES[UA_TYPES_INT32]);
    UA_Server_writeValue(server, myIntegerNodeId, myVar);*/
}

static void
addPubSubConnection(UA_Server *server){
	
    UA_PubSubConnectionConfig connectionConfig;
    memset(&connectionConfig, 0, sizeof(connectionConfig));
    connectionConfig.name = UA_STRING("OPC UA Initialization");
    connectionConfig.transportProfileUri =
    UA_STRING("http://opcfoundation.org/UA-Profile/Transport/pubsub-CoAP");
    connectionConfig.enabled = UA_TRUE;    
    UA_Variant_setScalar(&connectionConfig.address, &networkAddressUrl,
                         &UA_TYPES[UA_TYPES_NETWORKADDRESSURLDATATYPE]);
    connectionConfig.publisherId.numeric = UA_UInt32_random();
    
    UA_Server_addPubSubConnection(server, &connectionConfig, &connectionIdent);
   
}

static void
addPublishedDataSet(UA_Server *server) {
    /* The PublishedDataSetConfig contains all necessary public
    * informations for the creation of a new PublishedDataSet */
    UA_PublishedDataSetConfig publishedDataSetConfig;
    memset(&publishedDataSetConfig, 0, sizeof(UA_PublishedDataSetConfig));
    publishedDataSetConfig.publishedDataSetType = UA_PUBSUB_DATASET_PUBLISHEDITEMS;
    publishedDataSetConfig.name = UA_STRING("Demo PDS");
    /* Create new PublishedDataSet based on the PublishedDataSetConfig. */
    UA_Server_addPublishedDataSet(server, &publishedDataSetConfig, &publishedDataSetIdent);
}


static UA_StatusCode addVariable(UA_Server *server,
								UA_UInt16* ns,
								UA_UInt32 requestedNewNodeId,
								UA_UInt32 parentNodeId,
								char *browseName,
								char *description,
								UA_UInt32 dataType) {
	UA_StatusCode retVal = UA_STATUSCODE_GOOD;
	UA_VariableAttributes attr = UA_VariableAttributes_default;

	switch(dataType) {
		case 0: {	// BOOLEAN
			attr.dataType = UA_TYPES[UA_TYPES_BOOLEAN].typeId;
			UA_Boolean variablenode0 = false;
			UA_Variant_setScalarCopy(&attr.value, &variablenode0, &UA_TYPES[UA_TYPES_BOOLEAN]);
			break;
		}
		case 1:	{ 	// DOUBLE
			attr.dataType = UA_TYPES[UA_TYPES_DOUBLE].typeId;
			UA_Double variablenode1 = 0;
			UA_Variant_setScalarCopy(&attr.value, &variablenode1, &UA_TYPES[UA_TYPES_DOUBLE]);
			break;
		}
		case 2: {	// INTEGER
			attr.dataType = UA_TYPES[UA_TYPES_INT32].typeId;
			UA_Int32 variablenode2 = 0;
			UA_Variant_setScalarCopy(&attr.value, &variablenode2, &UA_TYPES[UA_TYPES_INT32]);
			break;
		}
		case 3: {	// STRING
			attr.dataType = UA_TYPES[UA_TYPES_STRING].typeId;
			UA_String variablenode3 = UA_STRING("Value is empty");;
			UA_Variant_setScalarCopy(&attr.value, &variablenode3, &UA_TYPES[UA_TYPES_STRING]);
			break;
		}
	}
	attr.displayName = UA_LOCALIZEDTEXT("en-US", browseName);
	attr.description = UA_LOCALIZEDTEXT("en-US", description);
	attr.accessLevel = UA_ACCESSLEVELMASK_READ | UA_ACCESSLEVELMASK_WRITE;

    UA_NodeId IntegerNodeId = UA_NODEID_NUMERIC(ns[0], requestedNewNodeId);
    UA_QualifiedName myIntegerName = UA_QUALIFIEDNAME(ns[0], browseName);
    UA_NodeId pNodeId = UA_NODEID_NUMERIC(ns[0], parentNodeId);
    UA_NodeId parentReferenceNodeId = UA_NODEID_NUMERIC(ns[0], UA_NS0ID_HASCOMPONENT);
    retVal |= UA_Server_addVariableNode(server, IntegerNodeId, pNodeId,
                              parentReferenceNodeId, myIntegerName,
                              UA_NODEID_NUMERIC(ns[0], UA_NS0ID_BASEDATAVARIABLETYPE), attr, NULL, NULL);
	
	return retVal;
}


/**
 * **DataSetField handling**
 * The DataSetField (DSF) is part of the PDS and describes exactly one published field.
 */
static void
addDataSetField(UA_Server *server) {
    /* Add a State and Speed Variable Nodes of BridgeDriveController Object as fields to the previous created PublishedDataSet */
    
    
    UA_NodeId dataSetFieldIdent;
    UA_DataSetFieldConfig dataSetFieldConfig;
    memset(&dataSetFieldConfig, 0, sizeof(UA_DataSetFieldConfig));
    dataSetFieldConfig.dataSetFieldType = UA_PUBSUB_DATASETFIELD_VARIABLE;

    dataSetFieldConfig.field.variable.fieldNameAlias = UA_STRING("State");
    dataSetFieldConfig.field.variable.promotedField = UA_FALSE;
    dataSetFieldConfig.field.variable.publishParameters.publishedVariable = UA_NODEID_NUMERIC(n, 1020);
    dataSetFieldConfig.field.variable.publishParameters.attributeId = UA_ATTRIBUTEID_VALUE;
    UA_Server_addDataSetField(server, publishedDataSetIdent, &dataSetFieldConfig, &dataSetFieldIdent);
	
    UA_NodeId dataSetFieldIdent1;
    UA_DataSetFieldConfig dataSetFieldConfig1;
    memset(&dataSetFieldConfig1, 0, sizeof(UA_DataSetFieldConfig));
    dataSetFieldConfig1.dataSetFieldType = UA_PUBSUB_DATASETFIELD_VARIABLE;

    dataSetFieldConfig1.field.variable.fieldNameAlias = UA_STRING("Speed");
    dataSetFieldConfig1.field.variable.promotedField = UA_FALSE;
    dataSetFieldConfig1.field.variable.publishParameters.publishedVariable = UA_NODEID_NUMERIC(n, 1021);
    dataSetFieldConfig1.field.variable.publishParameters.attributeId = UA_ATTRIBUTEID_VALUE;
    UA_Server_addDataSetField(server, publishedDataSetIdent, &dataSetFieldConfig1, &dataSetFieldIdent1);

/*Add a State and Speed Variable Nodes of CrabDriveController Object as fields to the previous created PublishedDataSet */

    UA_NodeId dataSetFieldIdent2;
    UA_DataSetFieldConfig dataSetFieldConfig2;
    memset(&dataSetFieldConfig2, 0, sizeof(UA_DataSetFieldConfig));
    dataSetFieldConfig2.dataSetFieldType = UA_PUBSUB_DATASETFIELD_VARIABLE;

    dataSetFieldConfig2.field.variable.fieldNameAlias = UA_STRING("State");
    dataSetFieldConfig2.field.variable.promotedField = UA_FALSE;
    dataSetFieldConfig2.field.variable.publishParameters.publishedVariable = UA_NODEID_NUMERIC(n, 1022);
    dataSetFieldConfig2.field.variable.publishParameters.attributeId = UA_ATTRIBUTEID_VALUE;
    UA_Server_addDataSetField(server, publishedDataSetIdent, &dataSetFieldConfig2, &dataSetFieldIdent2);
	
    UA_NodeId dataSetFieldIdent3;
    UA_DataSetFieldConfig dataSetFieldConfig3;
    memset(&dataSetFieldConfig3, 0, sizeof(UA_DataSetFieldConfig));
    dataSetFieldConfig3.dataSetFieldType = UA_PUBSUB_DATASETFIELD_VARIABLE;

    dataSetFieldConfig3.field.variable.fieldNameAlias = UA_STRING("Speed");
    dataSetFieldConfig3.field.variable.promotedField = UA_FALSE;
    dataSetFieldConfig3.field.variable.publishParameters.publishedVariable = UA_NODEID_NUMERIC(n, 1023);
    dataSetFieldConfig3.field.variable.publishParameters.attributeId = UA_ATTRIBUTEID_VALUE;
    UA_Server_addDataSetField(server, publishedDataSetIdent, &dataSetFieldConfig3, &dataSetFieldIdent3);

    /*Add a State and Speed Variable Nodes of HoistController Object as fields to the previous created PublishedDataSet */

    /*UA_NodeId dataSetFieldIdent4;
    UA_DataSetFieldConfig dataSetFieldConfig4;
    memset(&dataSetFieldConfig4, 0, sizeof(UA_DataSetFieldConfig));
    dataSetFieldConfig4.dataSetFieldType = UA_PUBSUB_DATASETFIELD_VARIABLE;

    dataSetFieldConfig4.field.variable.fieldNameAlias = UA_STRING("State");
    dataSetFieldConfig4.field.variable.promotedField = UA_FALSE;
    dataSetFieldConfig4.field.variable.publishParameters.publishedVariable = UA_NODEID_NUMERIC(n, 1024);
    dataSetFieldConfig4.field.variable.publishParameters.attributeId = UA_ATTRIBUTEID_VALUE;
    UA_Server_addDataSetField(server, publishedDataSetIdent, &dataSetFieldConfig4, &dataSetFieldIdent4);
	
    UA_NodeId dataSetFieldIdent5;
    UA_DataSetFieldConfig dataSetFieldConfig5;
    memset(&dataSetFieldConfig5, 0, sizeof(UA_DataSetFieldConfig));
    dataSetFieldConfig5.dataSetFieldType = UA_PUBSUB_DATASETFIELD_VARIABLE;

    dataSetFieldConfig5.field.variable.fieldNameAlias = UA_STRING("Speed");
    dataSetFieldConfig5.field.variable.promotedField = UA_FALSE;
    dataSetFieldConfig5.field.variable.publishParameters.publishedVariable = UA_NODEID_NUMERIC(n, 1025);
    dataSetFieldConfig5.field.variable.publishParameters.attributeId = UA_ATTRIBUTEID_VALUE;
    UA_Server_addDataSetField(server, publishedDataSetIdent, &dataSetFieldConfig5, &dataSetFieldIdent5);*/
}

/**
 * **WriterGroup handling**
 * The WriterGroup (WG) is part of the connection and contains the primary configuration
 * parameters for the message creation.
 */
static void
addWriterGroup(UA_Server *server, UA_PubSubConnection *connection) {
    /* Now we create a new WriterGroupConfig and add the group to the existing PubSubConnection. */
    UA_WriterGroupConfig writerGroupConfig;
    memset(&writerGroupConfig, 0, sizeof(UA_WriterGroupConfig));
    writerGroupConfig.name = UA_STRING("Demo WriterGroup");
    writerGroupConfig.publishingInterval = 3000; //initial value was 500
    writerGroupConfig.enabled = UA_FALSE;
    writerGroupConfig.writerGroupId = 100;
    
    /* Choose the encoding, UA_PUBSUB_ENCODING_JSON is available soon */
    writerGroupConfig.encodingMimeType = UA_PUBSUB_ENCODING_JSON; 
	//writerGroupConfig.encodingMimeType = UA_PUBSUB_ENCODING_UADP;
	//writerGroupConfig.encodingMimeType = UA_PUBSUB_ENCODING_BINARY;
    
    UA_BrokerWriterGroupTransportDataType brokerTransportSettings;
    memset(&brokerTransportSettings, 0, sizeof(UA_BrokerWriterGroupTransportDataType));
    brokerTransportSettings.queueName = UA_STRING("hello"); //a topic to publish data
    brokerTransportSettings.resourceUri = UA_STRING_NULL;
    brokerTransportSettings.authenticationProfileUri = UA_STRING_NULL;
    
    UA_ExtensionObject transportSettings;
    memset(&transportSettings, 0, sizeof(UA_ExtensionObject));
    transportSettings.encoding = UA_EXTENSIONOBJECT_DECODED;
    transportSettings.content.decoded.type = &UA_TYPES[UA_TYPES_BROKERWRITERGROUPTRANSPORTDATATYPE];
    transportSettings.content.decoded.data = &brokerTransportSettings;
    
    writerGroupConfig.transportSettings = transportSettings;
    /* The configuration flags for the messages are encapsulated inside the
     * message- and transport settings extension objects. These extension objects
     * are defined by the standard. e.g. UadpWriterGroupMessageDataType */

	UA_PubSubChannelDataCoAP * channelData =
            (UA_PubSubChannelDataCoAP *) UA_calloc(1, (sizeof(UA_PubSubChannelDataCoAP)));
	connection->channel->handle = channelData;
	connection->channel->state = UA_PUBSUB_CHANNEL_PUB_SUB;    
	
    UA_Server_addWriterGroup(server, connectionIdent, &writerGroupConfig, &writerGroupIdent);
}

/**
 * **DataSetWriter handling**
 * A DataSetWriter (DSW) is the glue between the WG and the PDS. The DSW is linked to exactly one
 * PDS and contains additional informations for the message generation.
 */
static void
addDataSetWriter(UA_Server *server) {
    /* We need now a DataSetWriter within the WriterGroup. This means we must
     * create a new DataSetWriterConfig and add call the addWriterGroup function. */
    UA_NodeId dataSetWriterIdent;
    UA_DataSetWriterConfig dataSetWriterConfig;
    memset(&dataSetWriterConfig, 0, sizeof(UA_DataSetWriterConfig));
    dataSetWriterConfig.name = UA_STRING("Demo DataSetWriter");
    dataSetWriterConfig.dataSetWriterId = 62541;
    dataSetWriterConfig.keyFrameCount = 10;
    
    /* JSON config for the dataSetWriter */
    UA_JsonDataSetWriterMessageDataType jsonDswMd;
    jsonDswMd.dataSetMessageContentMask = (UA_JsonDataSetMessageContentMask)
            (UA_JSONDATASETMESSAGECONTENTMASK_DATASETWRITERID 
            | UA_JSONDATASETMESSAGECONTENTMASK_SEQUENCENUMBER
            | UA_JSONDATASETMESSAGECONTENTMASK_STATUS
            | UA_JSONDATASETMESSAGECONTENTMASK_METADATAVERSION
            | UA_JSONDATASETMESSAGECONTENTMASK_TIMESTAMP);
    
    UA_ExtensionObject messageSettings;
    messageSettings.encoding = UA_EXTENSIONOBJECT_DECODED;
    messageSettings.content.decoded.type = &UA_TYPES[UA_TYPES_JSONDATASETWRITERMESSAGEDATATYPE];
	//messageSettings.content.decoded.type = &UA_TYPES[UA_TYPES_UADPDATASETWRITERMESSAGEDATATYPE];
    messageSettings.content.decoded.data = &jsonDswMd;
	messageSettings.content.decoded.data = NULL;
	
    
    dataSetWriterConfig.messageSettings = messageSettings;
    
    
    UA_Server_addDataSetWriter(server, writerGroupIdent, publishedDataSetIdent,
                               &dataSetWriterConfig, &dataSetWriterIdent);
}

UA_Boolean running = true;
static void stopHandler(int sign) {
    UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "received ctrl-c");
    running = false;
}


int main(int argc, char **argv) {
    signal(SIGINT, stopHandler);
    signal(SIGTERM, stopHandler);

    networkAddressUrl.networkInterface = UA_STRING_NULL;
    networkAddressUrl.url = UA_STRING("opc.coap://127.0.0.1:5683/");

    if(argc > 1 && strncmp(argv[1], "opc.coap://", 11) == 0) {
        networkAddressUrl.url = UA_STRING(argv[1]);
    }

    UA_StatusCode retval = UA_STATUSCODE_GOOD;
    UA_ServerConfig *config = UA_ServerConfig_new_default();
    /* Details about the connection configuration and handling are located in
       the pubsub connection tutorial */
    config->pubsubTransportLayers = (UA_PubSubTransportLayer *) UA_malloc(1  * sizeof(UA_PubSubTransportLayer));
    if(!config->pubsubTransportLayers) {
        UA_ServerConfig_delete(config);
        return -1;
    }

    config->pubsubTransportLayers[0] = UA_PubSubTransportLayerCoAP();
    config->pubsubTransportLayersSize++;
    
   UA_Server *server = UA_Server_new(config);	
  UA_UInt16 ns[2];
	ns[0] = UA_Server_addNamespace(server, "http://opcfoundation.org/UA/");
	ns[1] = UA_Server_addNamespace(server, "http://OPTIMUM/UA/");
	
	n = UA_Server_addNamespace(server, "http://opcfoundation.org/UA/");

  //--------------------------adding objects in information model-------------------------------
   for(int i=0; i<NUM_OBJECTS; i++)
		retval |= addObject(server, ns,
							nodeId[i],
							nodeParentId[i],
							nodeReferenceTypeId[i],
							nodeName[i],
							nodeDescription[i]);
  //--------------------------end of adding objects in information model-------------------------------

    //addVariable(server);
  //--------------------------adding variables in information model-------------------------------	
	for(int i=0; i<NUM_VARIABLES; i++)
		retval |= addVariable(server, ns,
							variableId[i],
							variableParentId[i],
							variableName[i],
							"Empty description for variable",
							variableDataType[i]);
  //--------------------------end of adding variables in information model-------------------------------
	
    addPubSubConnection(server); 
    addPublishedDataSet(server);
    addDataSetField(server);
  
    UA_PubSubConnection *connection =
        UA_PubSubConnection_findConnectionbyId(server, connectionIdent);
	
	if(connection != NULL) {
		UA_UInt64 subscriptionCallbackId;
		UA_Server_addRepeatedCallback(server, (UA_ServerCallback)PollingCallback,
                                          connection, 3000, &subscriptionCallbackId);
    }
    addValueCallbackToCurrentTimeVariable(server);
    addWriterGroup(server, connection);
    addDataSetWriter(server);    
	
    retval |= UA_Server_run(server, &running);
	
    UA_Server_delete(server);
    UA_ServerConfig_delete(config);
	
    return (int)retval;
}

