  
#include <stdio.h>
#include <stdlib.h>
#include <memory.h>
#include <stdio.h>
#include <signal.h>

#include <sys/time.h>

#include "ua_pubsub_networkmessage.h"
#include "ua_log_stdout.h"
#include "ua_server.h"
#include "ua_config_default.h"
#include "ua_pubsub.h"
#include "ua_network_pubsub_coap.h"
#include "src_generated/ua_types_generated.h"
#include "ua_types.h"
#include <signal.h>
#include <unistd.h>

UA_NodeId connectionIdent, publishedDataSetIdent, writerGroupIdent;

UA_NetworkAddressUrlDataType networkAddressUrl;

static void callback(UA_ByteString *encodedBuffer){
     
     
     UA_NetworkMessage dst;
	 memset(&dst, 0, sizeof(UA_NetworkMessage));
    UA_StatusCode ret = UA_NetworkMessage_decodeJson(&dst, encodedBuffer);
	
     if( ret == UA_STATUSCODE_GOOD){
		 	UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND,
    	    	    	    	                "Message Status is good");
     }
	 else {
		 printf("Received default payload : %s\n", (*encodedBuffer).data);
		 return;
	 }
      
	     /* At least one DataSetMessage in the NetworkMessage? */
    if(dst.payloadHeaderEnabled &&
       dst.payloadHeader.dataSetPayloadHeader.count < 1){
    	 UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND,
    	    	    	                "check the count of message");
        goto cleanup;
		}

    /* Is this a KeyFrame-DataSetMessage? ..................................................................*/
    UA_DataSetMessage *dsm = &dst.payload.dataSetPayload.dataSetMessages[0];
    if(dsm->header.dataSetMessageType != UA_DATASETMESSAGE_DATAKEYFRAME){

    	UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND,
    	    	    	    	                "Message type is not data key frame");
        goto cleanup;
	}

    /* Loop over the fields and print well-known content types */

    for(int i = 0; i < dsm->data.keyFrameData.fieldCount; i++) {
	
        const UA_DataType *currentType = dsm->data.keyFrameData.dataSetFields[i].value.type;
        if(currentType == &UA_TYPES[UA_TYPES_INT32]) {
            UA_Int32 value = *(UA_Int32 *)dsm->data.keyFrameData.dataSetFields[i].value.data;
            UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND,
                        "Message content: [UA_Int32] \tReceived Payload data: %d", value);
        } else if (currentType == &UA_TYPES[UA_TYPES_DATETIME]) {
            UA_DateTime value = *(UA_DateTime *)dsm->data.keyFrameData.dataSetFields[i].value.data;
            UA_DateTimeStruct receivedTime = UA_DateTime_toStruct(value);
            UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND,
                        "Message content: [DateTime] \t"
                        "Received date: %02i-%02i-%02i Received time: %02i:%02i:%02i",
                        receivedTime.year, receivedTime.month, receivedTime.day,
                        receivedTime.hour, receivedTime.min, receivedTime.sec);
        }
    }

 cleanup:{
    UA_ByteString_delete(encodedBuffer);
    UA_NetworkMessage_deleteMembers(&dst);
	}
}

static void
addPubSubConnection(UA_Server *server){
	
    UA_PubSubConnectionConfig connectionConfig;
    memset(&connectionConfig, 0, sizeof(connectionConfig));
    connectionConfig.name = UA_STRING("OPC UA Initialization");
    connectionConfig.transportProfileUri =
    UA_STRING("http://opcfoundation.org/UA-Profile/Transport/pubsub-CoAP");
    connectionConfig.enabled = UA_TRUE;    
    UA_Variant_setScalar(&connectionConfig.address, &networkAddressUrl,
                         &UA_TYPES[UA_TYPES_NETWORKADDRESSURLDATATYPE]);
    connectionConfig.publisherId.numeric = UA_UInt32_random();
	
	 UA_KeyValuePair connectionOptions[1];
    connectionOptions[0].key = UA_QUALIFIEDNAME(0, "coapClientId");
    UA_String coapClientId = UA_STRING("hello");
	
    UA_Variant_setScalar(&connectionOptions[0].value, &coapClientId, &UA_TYPES[UA_TYPES_STRING]);
	
    
    connectionConfig.connectionProperties = connectionOptions;
    connectionConfig.connectionPropertiesSize = 1;
   
    UA_Server_addPubSubConnection(server, &connectionConfig, &connectionIdent); 
	
}

static void
addRead(UA_PubSubConnection *connection){
   //Register Transport settings
    UA_BrokerWriterGroupTransportDataType brokerTransportSettings;
    memset(&brokerTransportSettings, 0, sizeof(UA_BrokerWriterGroupTransportDataType));
    brokerTransportSettings.queueName = UA_STRING(".well-known/core");
    brokerTransportSettings.resourceUri = UA_STRING_NULL;
    brokerTransportSettings.authenticationProfileUri = UA_STRING_NULL;

    UA_ExtensionObject transportSettings;
    memset(&transportSettings, 0, sizeof(UA_ExtensionObject));
    transportSettings.encoding = UA_EXTENSIONOBJECT_DECODED;
    transportSettings.content.decoded.type = &UA_TYPES[UA_TYPES_BROKERWRITERGROUPTRANSPORTDATATYPE];
    transportSettings.content.decoded.data = &brokerTransportSettings;

	UA_PubSubChannelDataCoAP * channelData =
            (UA_PubSubChannelDataCoAP *) UA_calloc(1, (sizeof(UA_PubSubChannelDataCoAP)));
	channelData->callback = &callback;
	connection->channel->handle = channelData;

    UA_StatusCode rv = connection->channel->receive(connection->channel, NULL, &transportSettings, NULL); // read a topic payload
}

static void
deleteTopic(UA_PubSubConnection *connection){
	
	UA_String topic = UA_STRING("hello"); // topic to delete
		
	UA_PubSubChannelDataCoAP * channelData = (UA_PubSubChannelDataCoAP *) UA_calloc(1, (sizeof(UA_PubSubChannelDataCoAP)));
	
	channelData->topic = topic;
	channelData->callback = &callback;
	connection->channel->state = UA_PUBSUB_CHANNEL_PUB_SUB;
	connection->channel->handle = channelData;
	
    UA_StatusCode rv = connection->channel->close(connection->channel); // delete a topic
		
}

/* Removes a subscription */
static void 
removeSubscription(UA_PubSubConnection *connection){
    
    //Register Transport settings
    UA_BrokerWriterGroupTransportDataType brokerTransportSettings;
    memset(&brokerTransportSettings, 0, sizeof(UA_BrokerWriterGroupTransportDataType));
    brokerTransportSettings.queueName = UA_STRING("hello");
    brokerTransportSettings.resourceUri = UA_STRING_NULL;
    brokerTransportSettings.authenticationProfileUri = UA_STRING_NULL;

    UA_ExtensionObject transportSettings;
    memset(&transportSettings, 0, sizeof(UA_ExtensionObject));
    transportSettings.encoding = UA_EXTENSIONOBJECT_DECODED;
    transportSettings.content.decoded.type = &UA_TYPES[UA_TYPES_BROKERWRITERGROUPTRANSPORTDATATYPE];
    transportSettings.content.decoded.data = &brokerTransportSettings;

	UA_PubSubChannelDataCoAP * channelData =
            (UA_PubSubChannelDataCoAP *) UA_calloc(1, (sizeof(UA_PubSubChannelDataCoAP)));
	channelData->callback = &callback;
	connection->channel->handle = channelData;
	
    UA_StatusCode rv = connection->channel->unregist(connection->channel, &transportSettings); // to unsubscribe to a topic

    if (rv == UA_STATUSCODE_GOOD) {
            UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "Unsubscribed");
        } else {
            UA_LOG_WARNING(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "register channel failed: %s!",
                           UA_StatusCode_name(rv));
    }
    
    return; 
}

/* Periodically refreshes the CoAP stack for getting published payload*/
static void
CoAPYieldPollingCallback(UA_Server *server, UA_PubSubConnection *connection) {
	//UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "Polling");
    connection->channel->yield(connection->channel);
	
}

/* Adds a subscription */
static void 
addSubscription(UA_Server *server, UA_PubSubConnection *connection){
    
    //Register Transport settings
    UA_BrokerWriterGroupTransportDataType brokerTransportSettings;
    memset(&brokerTransportSettings, 0, sizeof(UA_BrokerWriterGroupTransportDataType));
    brokerTransportSettings.queueName = UA_STRING("hello");
    brokerTransportSettings.resourceUri = UA_STRING_NULL;
    brokerTransportSettings.authenticationProfileUri = UA_STRING_NULL;

    UA_ExtensionObject transportSettings;
    memset(&transportSettings, 0, sizeof(UA_ExtensionObject));
    transportSettings.encoding = UA_EXTENSIONOBJECT_DECODED;
    transportSettings.content.decoded.type = &UA_TYPES[UA_TYPES_BROKERWRITERGROUPTRANSPORTDATATYPE];
    transportSettings.content.decoded.data = &brokerTransportSettings;

	UA_PubSubChannelDataCoAP * channelData =
            (UA_PubSubChannelDataCoAP *) UA_calloc(1, (sizeof(UA_PubSubChannelDataCoAP)));
	channelData->callback = &callback;
	connection->channel->handle = channelData;
	
    UA_StatusCode rv = connection->channel->regist(connection->channel, &transportSettings, &callback); //subscription to a topic
	
    if (rv == UA_STATUSCODE_GOOD) {
            UA_UInt64 subscriptionCallbackId;
            UA_Server_addRepeatedCallback(server, (UA_ServerCallback)CoAPYieldPollingCallback,
                                          connection, 200, &subscriptionCallbackId);
        } else {
            UA_LOG_WARNING(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "register channel failed: %s!",
                           UA_StatusCode_name(rv));
    }
    
    return; 
}

UA_Boolean running = true;
static void stopHandler(int sign) {
    UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "received ctrl-c");
    running = false;
}


int main(int argc, char **argv) {
    signal(SIGINT, stopHandler);
    signal(SIGTERM, stopHandler);

    networkAddressUrl.networkInterface = UA_STRING_NULL;
    networkAddressUrl.url = UA_STRING("opc.coap://127.0.0.1:5683/");
    if(argc > 1 && strncmp(argv[1], "opc.coap://", 11) == 0) {
        networkAddressUrl.url = UA_STRING(argv[1]);
    }

    UA_StatusCode retval = UA_STATUSCODE_GOOD;
    UA_ServerConfig *config = UA_ServerConfig_new_minimal(4801, NULL);
 
    config->pubsubTransportLayers = (UA_PubSubTransportLayer *) UA_malloc(1  * sizeof(UA_PubSubTransportLayer));
    if(!config->pubsubTransportLayers) {
        UA_ServerConfig_delete(config);
        return -1;
    }
	
    config->pubsubTransportLayers[0] = UA_PubSubTransportLayerCoAP();
    config->pubsubTransportLayersSize++;
	
	
   UA_Server *server = UA_Server_new(config);
    
    addPubSubConnection(server);    

    UA_PubSubConnection *connection =
        UA_PubSubConnection_findConnectionbyId(server, connectionIdent);

    if(connection != NULL) {
	//addRead(connection);
        addSubscription(server, connection);
	//removeSubscription(connection);
	//deleteTopic(connection);		
    }
   
    retval |= UA_Server_run(server, &running);
	
    UA_Server_delete(server);
    UA_ServerConfig_delete(config);
    return (int)retval;
}

