
 /* Assume that Windows versions are newer than Windows XP */
#if defined(__MINGW32__) && (!defined(WINVER) || WINVER < 0x501)
# undef WINVER
# undef _WIN32_WINDOWS
# undef _WIN32_WINNT
# define WINVER 0x0501
# define _WIN32_WINDOWS 0x0501
# define _WIN32_WINNT 0x0501
#endif

#ifdef _WIN32
# include <winsock2.h>
# include <ws2tcpip.h>
# include <Iphlpapi.h>
# define CLOSESOCKET(S) closesocket((SOCKET)S)
# define ssize_t int
# define UA_fd_set(fd, fds) FD_SET((unsigned int)fd, fds)
# define UA_fd_isset(fd, fds) FD_ISSET((unsigned int)fd, fds)
#else /* _WIN32 */
#  define CLOSESOCKET(S) close(S)
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <net/if.h>
# endif /* Not Windows */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "/usr/local/include/coap2/coap.h"
#include "/usr/local/include/coap2/net.h"
#include "/usr/local/include/coap2/coap_session.h"

#include "ua_plugin_network.h"
//#include "ua_network_pubsub_mqtt.h"
#include "ua_network_pubsub_coap.h"
//#include "mqtt/ua_mqtt_adapter.h"
#include "ua_log_stdout.h"
#include <ua_network_tcp.h>
#include <ua_util.h>


UA_PubSubChannelDataCoAP * channelData;
coap_context_t*   ctx;
coap_context_t* context;
coap_address_t    dst_addr, src_addr;
coap_session_t *session;
static coap_uri_t uri;
fd_set            readfds, rfds; 
unsigned wait_ms = COAP_RESOURCE_CHECK_TIME * 1000;
int coap_fd, result, res, cfd; 
coap_pdu_t*       request;
const char*       server_uri = NULL;
const char* default_address = "coap://127.0.0.1/";
UA_NetworkAddressUrlDataType address;


static void prepare_request(){
	
	coap_address_init(&src_addr);
	src_addr.addr.sin.sin_family      = AF_INET;
	src_addr.addr.sin.sin_port        = htons(0);
	src_addr.addr.sin.sin_addr.s_addr = INADDR_ANY;
	ctx = coap_new_context(&src_addr);
	coap_fd = coap_context_get_coap_fd(ctx); 

	/* The destination endpoint */
	coap_address_init(&dst_addr);
	dst_addr.addr.sin.sin_family      = AF_INET;
	dst_addr.addr.sin.sin_port        = htons(5683);
	dst_addr.addr.sin.sin_addr.s_addr = inet_addr("127.0.0.1");

	/* Prepare the request */
	coap_split_uri(server_uri, strlen(server_uri), &uri);
	session = coap_new_client_session(ctx, &src_addr, &dst_addr, COAP_PROTO_UDP);
	request            = coap_new_pdu(session); 
	request->type = COAP_MESSAGE_CON;
	request->tid   = coap_new_message_id(session);
}

static void
message_handler(struct coap_context_t *ctx, coap_session_t *session, 
                coap_pdu_t *sent, coap_pdu_t *received, 
                const coap_tid_t id) 
{
	unsigned char* data;
	size_t         data_len;

	//printf("Handler called \n");	
	
	if (COAP_RESPONSE_CLASS(received->code) == 2) {
		if (coap_get_data(received, &data_len, &data))
		{
			printf("Received: %s\n", data);
			/*if(channelData != NULL){
				if(channelData->callback != NULL){       
					UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "before  UA_ByteString_new");
					UA_ByteString *msg = UA_ByteString_new(); 
					UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "after UA_ByteString_new");
					UA_StatusCode ret = UA_ByteString_allocBuffer(msg, data_len);
					UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "after UA_ByteString_allocBuffer");
					if(ret){
						free(msg); 
						return;
					}
					 UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "before  UA_ByteString_new memcpy()");
					memcpy(msg->data, data, data_len);
					UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "after  UA_ByteString_new memcpy()");
					channelData->callback(msg);
				}
			} */
		}
	}
	
	if(channelData != NULL){
		if(channelData->callback != NULL){       
			
			/* callback with message and topic as bytestring. */
			//UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "before  UA_ByteString_new");
			UA_ByteString *msg = UA_ByteString_new(); 
			//UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "after UA_ByteString_new");
			UA_StatusCode ret = UA_ByteString_allocBuffer(msg, data_len);
			//UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "after UA_ByteString_allocBuffer");
			if(ret){
				free(msg); 
				return;
			}
			 //UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "before  UA_ByteString_new memcpy()");
			memcpy(msg->data, data, data_len);
			//UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "after  UA_ByteString_new memcpy()");
			channelData->callback(msg);
		}
	} 
	 
}

static void socket_read(){
	//UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "get_updates invoked");
	FD_ZERO(&readfds);
	FD_SET( coap_fd, &readfds );
	struct timeval tv;
	tv.tv_sec = wait_ms/1000;
	tv.tv_usec = (wait_ms%1000)*1000;
	result = select( coap_fd + 1, &readfds, 0, 0, &tv );
	
	if ( result < 0 ) /* socket error */
	{
		exit(EXIT_FAILURE);
	} 
	else if ( result > 0 && FD_ISSET(coap_fd, &readfds )) /* socket read*/
	{	//UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "before coap_run_once"); 
		coap_run_once( ctx, wait_ms );      
	} 
}

static void UA_PubSubChannelCoAP_get_updates(UA_PubSubChannel *channel){
	UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "UA_PubSubChannelCoAP_get_updates invoked");
	//printf("Result value: %d\n", res);	
	//socket_read(res, context, rfds, wait_ms, cfd);
	socket_read();
}

static UA_PubSubChannel *
UA_PubSubChannelCoAP_Open(const UA_PubSubConnectionConfig *connectionConfig) {  

    if(UA_Variant_hasScalarType(&connectionConfig->address, &UA_TYPES[UA_TYPES_NETWORKADDRESSURLDATATYPE])){
        address = *(UA_NetworkAddressUrlDataType *)connectionConfig->address.data;
    } else {
        UA_LOG_ERROR(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "PubSub Connection creation failed. Invalid Address.");
        return NULL;
    }	
    UA_String hostname, path;
    UA_UInt16 networkPort;
    if(UA_parseEndpointUrl(&address.url, &hostname, &networkPort, &path) != UA_STATUSCODE_GOOD){
        UA_LOG_ERROR(UA_Log_Stdout, UA_LOGCATEGORY_SERVER,
                     "PubSub Connection creation failed. Invalid URL.");
        return UA_STATUSCODE_BADINVALIDARGUMENT;
    } 

    /* Create a new channel */
    UA_PubSubChannel *newChannel = (UA_PubSubChannel *) UA_calloc(1, sizeof(UA_PubSubChannel));
    if(!newChannel){
        UA_LOG_ERROR(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "PubSub Connection creation failed. Out of memory.");
        return NULL;
    }
    
    UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "CoAP Connection established.");
    
    newChannel->state = UA_PUBSUB_CHANNEL_RDY;
    return newChannel;
}

static void
UA_PubSubChannelCoAP_Get(UA_PubSubChannel *channel, UA_ByteString *topic, UA_ExtensionObject *transportSettings, UA_UInt32 timeout) {
   
        UA_BrokerWriterGroupTransportDataType *brokerTransportSettings = (UA_BrokerWriterGroupTransportDataType*)transportSettings->content.decoded.data;

    channelData = (UA_PubSubChannelDataCoAP *) channel->handle;
    Read(channelData, (const char*)brokerTransportSettings->queueName.data);
     
    printf("Get Request Sent..\n");
    
}

static void
UA_PubSubChannelCoAP_Subscribe(UA_PubSubChannel *channel, UA_ExtensionObject *transportSettigns, void (*callback)()) {
    if(!(channel->state == UA_PUBSUB_CHANNEL_PUB_SUB || channel->state == UA_PUBSUB_CHANNEL_RDY)){
        UA_LOG_ERROR(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "PubSub Connection subscribe failed.");
        return UA_STATUSCODE_BADINTERNALERROR;
    }
    UA_StatusCode ret = UA_STATUSCODE_GOOD;
    
    channelData = (UA_PubSubChannelDataCoAP *) channel->handle;
    channelData->callback = callback;
    
     if(transportSettigns != NULL && transportSettigns->encoding == UA_EXTENSIONOBJECT_DECODED 
            && transportSettigns->content.decoded.type->typeIndex == UA_TYPES_BROKERWRITERGROUPTRANSPORTDATATYPE){
        UA_BrokerWriterGroupTransportDataType *brokerTransportSettings = (UA_BrokerWriterGroupTransportDataType*)transportSettigns->content.decoded.data;
	
	UA_String topic;
        topic = brokerTransportSettings->queueName;

	default_address = "coap://127.0.0.1/ps/";
	unsigned char buf[3];
	static unsigned int token = 2;
        server_uri = malloc(strlen(default_address) + strlen((const char*)topic.data));
	strcpy(server_uri, default_address);
	strcat(server_uri, (const char*)topic.data);

	prepare_request();

	request->code = COAP_REQUEST_GET; // for put request
	coap_add_token(request, sizeof(token), (unsigned char*)&token);

	coap_add_option(request, COAP_OPTION_OBSERVE, COAP_OBSERVE_ESTABLISH, NULL);// to subscribe to a resource
	//coap_add_option(request, COAP_OPTION_OBSERVE, coap_encode_var_safe(b, sizeof(b), COAP_OBSERVE_CANCEL), b); // to unsubscribe
	coap_add_option(request, COAP_OPTION_URI_PATH, uri.path.length, uri.path.s);
	coap_add_option(request, COAP_OPTION_CONTENT_TYPE, coap_encode_var_safe(buf, sizeof(buf), COAP_MEDIATYPE_APPLICATION_JSON), buf);

	/* Set the handler and send the request */
	//coap_register_response_handler(context, message_handler);//same
	coap_register_response_handler(ctx, message_handler);//same	
	
	coap_send(session, request);
	
	socket_read();

	/*FD_ZERO(&readfds);
	FD_SET( coap_fd, &readfds );
	struct timeval tv;
	tv.tv_sec = wait_ms/1000;
	tv.tv_usec = (wait_ms%1000)*1000;
	int result = select( coap_fd + 1, &readfds, 0, 0, &tv );
	printf("Result value in subscribe: %d\n", result);

	if ( result < 0 ) 
	{
		exit(EXIT_FAILURE);
	} 
	else if ( result > 0 && FD_ISSET(coap_fd, &readfds ))
	{	 
		coap_run_once( ctx, wait_ms ); 
		UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "Subscribed");     
	}    
	res = result;
	rfds = readfds;
	cfd = coap_fd;
	context = ctx;     
           */
    }
    
}

static UA_StatusCode
UA_PubSubChannelCoAP_Publish(UA_PubSubChannel *channel, UA_ExtensionObject *transportSettings, const UA_ByteString *buffer) {

    channelData = (UA_PubSubChannelDataCoAP *) channel->handle;

    if(!(channel->state == UA_PUBSUB_CHANNEL_PUB || channel->state == UA_PUBSUB_CHANNEL_PUB_SUB)){
        UA_LOG_WARNING(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "PubSub Connection sending failed. Invalid state.");
        return UA_STATUSCODE_BADINTERNALERROR;
    }
    
    UA_StatusCode ret = UA_STATUSCODE_GOOD;
    
    if(transportSettings != NULL && transportSettings->encoding == UA_EXTENSIONOBJECT_DECODED 
            && transportSettings->content.decoded.type->typeIndex == UA_TYPES_BROKERWRITERGROUPTRANSPORTDATATYPE){
        UA_BrokerWriterGroupTransportDataType *brokerTransportSettings = (UA_BrokerWriterGroupTransportDataType*)transportSettings->content.decoded.data;
        
        UA_String topic;
        topic = brokerTransportSettings->queueName;

	default_address = "coap://127.0.0.1/ps/";
	unsigned char buf[3];
        server_uri = malloc(strlen(default_address) + strlen((const char*)topic.data));
	strcpy(server_uri, default_address);
	strcat(server_uri, (const char*)topic.data);
	//const char*	post_data = "1333.33"; 
	const char*	post_data = (*buffer).data; // payload to send for put request
	
	//printf("post_data: %s\n",(unsigned char *)post_data);
	//printf("Concaneted String: %s\n",server_uri);
	//printf("buffer data in ua_network_pubsub_coap.c %s\n",(*buffer).data);
	prepare_request();

	request->code = COAP_REQUEST_PUT; // for put request
	coap_add_option(request, COAP_OPTION_URI_PATH, uri.path.length, uri.path.s);
	coap_add_option(request, COAP_OPTION_CONTENT_TYPE, coap_encode_var_safe(buf, sizeof(buf), COAP_MEDIATYPE_APPLICATION_JSON), buf);
	coap_add_data(request, strlen(post_data), (unsigned char *)post_data); // for put request

	/* Set the handler and send the request */
	coap_register_response_handler(ctx, message_handler);//same	
	coap_send(session, request);
	
	socket_read();
	
	printf("Publish Request Sent..\n");

	/*FD_ZERO(&readfds);
	FD_SET( coap_fd, &readfds );
	struct timeval tv;
	tv.tv_sec = wait_ms/1000;
	tv.tv_usec = (wait_ms%1000)*1000;
	result = select( coap_fd + 1, &readfds, 0, 0, &tv );
	if ( result < 0 ) 
	{
		exit(EXIT_FAILURE);
	} 
	else if ( result > 0 && FD_ISSET(coap_fd, &readfds )) 
	{	 
		coap_run_once( ctx, wait_ms );      
	}         
        UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "Publish");
        
    }else{
        UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "Transport settings not found.");
    }*/
	}
    return ret;
}

static void
UA_PubSubChannelCoAP_UnSubscribe(UA_PubSubChannel *channel, UA_ExtensionObject *transportSettigns) {
	
    if(!(channel->state == UA_PUBSUB_CHANNEL_PUB_SUB || channel->state == UA_PUBSUB_CHANNEL_RDY)){
        UA_LOG_ERROR(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "PubSub Connection subscribe failed.");
        return UA_STATUSCODE_BADINTERNALERROR;
    }
    UA_StatusCode ret = UA_STATUSCODE_GOOD;
    
    channelData = (UA_PubSubChannelDataCoAP *) channel->handle;
    //channelData->callback = callback;
    
     if(transportSettigns != NULL && transportSettigns->encoding == UA_EXTENSIONOBJECT_DECODED 
            && transportSettigns->content.decoded.type->typeIndex == UA_TYPES_BROKERWRITERGROUPTRANSPORTDATATYPE){
        UA_BrokerWriterGroupTransportDataType *brokerTransportSettings = (UA_BrokerWriterGroupTransportDataType*)transportSettigns->content.decoded.data;
	
	UA_String topic;
        topic = brokerTransportSettings->queueName;

	default_address = "coap://127.0.0.1/ps/";
	unsigned char buf[3];
	static unsigned int token = 2;
        server_uri = malloc(strlen(default_address) + strlen((const char*)topic.data));
	strcpy(server_uri, default_address);
	strcat(server_uri, (const char*)topic.data);

	prepare_request();

	request->code = COAP_REQUEST_GET; // for put request
	coap_add_token(request, sizeof(token), (unsigned char*)&token);

	coap_add_option(request, COAP_OPTION_OBSERVE, coap_encode_var_safe(buf, sizeof(buf), COAP_OBSERVE_CANCEL), buf); // to unsubscribe
	coap_add_option(request, COAP_OPTION_URI_PATH, uri.path.length, uri.path.s);
	coap_add_option(request, COAP_OPTION_CONTENT_TYPE, coap_encode_var_safe(buf, sizeof(buf), COAP_MEDIATYPE_APPLICATION_JSON), buf);

	/* Set the handler and send the request */
	//coap_register_response_handler(context, message_handler);//same
	coap_register_response_handler(ctx, message_handler);//same	
	
	coap_send(session, request);
	
	socket_read();

	/*FD_ZERO(&readfds);
	FD_SET( coap_fd, &readfds );
	struct timeval tv;
	tv.tv_sec = wait_ms/1000;
	tv.tv_usec = (wait_ms%1000)*1000;
	int result = select( coap_fd + 1, &readfds, 0, 0, &tv );
	//printf("Result value in unsubscribe: %d\n", result);

	if ( result < 0 ) 
	{
		exit(EXIT_FAILURE);
	} 
	else if ( result > 0 && FD_ISSET(coap_fd, &readfds )) 
	{	 
		coap_run_once( ctx, wait_ms ); 
		UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "UnSubscribed");     
	} */
           
    }
    
}

static UA_StatusCode
UA_PubSubChannelCoAP_Delete(UA_PubSubChannel *channel) {
  
    channelData = (UA_PubSubChannelDataCoAP *) channel->handle;
	
	 if(!(channel->state == UA_PUBSUB_CHANNEL_PUB || channel->state == UA_PUBSUB_CHANNEL_PUB_SUB)){
        UA_LOG_WARNING(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "PubSub Connection sending failed. Invalid state.");
        return UA_STATUSCODE_BADINTERNALERROR;
    }
    UA_String topic = channelData->topic;
	default_address = "coap://127.0.0.1/ps/";
	unsigned char buf[3];
    server_uri = malloc(strlen(default_address) + strlen(topic.data));
	strcpy(server_uri, default_address);
	strcat(server_uri, topic.data);
	
	//printf("Concaneted String: %s\n",server_uri);
	
	prepare_request();

	request->code = COAP_REQUEST_DELETE; // for put request
	coap_add_option(request, COAP_OPTION_URI_PATH, uri.path.length, uri.path.s);
	coap_add_option(request, COAP_OPTION_CONTENT_TYPE, coap_encode_var_safe(buf, sizeof(buf), COAP_MEDIATYPE_APPLICATION_JSON), buf);

	/* Set the handler and send the request */
	coap_register_response_handler(ctx, message_handler);//same	
	coap_send(session, request);
	
	socket_read();

	printf("Delete Request Sent..\n");

    UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "Deleted");
        
    
    return UA_STATUSCODE_GOOD;
}

/**
 * Generate a new channel. based on the given configuration.
 *
 * @param connectionConfig connection configuration
 * @return  ref to created channel, NULL on error
 */
static UA_PubSubChannel *
TransportLayerCoAP_addChannel(UA_PubSubConnectionConfig *connectionConfig) {
    UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "PubSub channel requested");
    UA_PubSubChannel * pubSubChannel = UA_PubSubChannelCoAP_Open(connectionConfig);
	if(pubSubChannel){
		pubSubChannel->regist = UA_PubSubChannelCoAP_Subscribe;
		pubSubChannel->unregist = UA_PubSubChannelCoAP_UnSubscribe;
		pubSubChannel->send = UA_PubSubChannelCoAP_Publish;
		pubSubChannel->receive = UA_PubSubChannelCoAP_Get;
		pubSubChannel->close = UA_PubSubChannelCoAP_Delete;
		pubSubChannel->yield = UA_PubSubChannelCoAP_get_updates;
	}
 	pubSubChannel->connectionConfig = connectionConfig;
    return pubSubChannel;
}

//CoAP channel factory
UA_PubSubTransportLayer
UA_PubSubTransportLayerCoAPUDPMP(){
    UA_PubSubTransportLayer pubSubTransportLayer;
    pubSubTransportLayer.transportProfileUri = UA_STRING("http://opcfoundation.org/UA-Profile/Transport/pubsub-CoAP"); 
    pubSubTransportLayer.createPubSubChannel = &TransportLayerCoAP_addChannel;
    return pubSubTransportLayer;
}

#undef _POSIX_C_SOURCE
