/* This work is licensed under a Creative Commons CCZero 1.0 Universal License.
 * See http://creativecommons.org/publicdomain/zero/1.0/ for more information.
 * 
 *    Copyright 2018 (c) Fraunhofer IOSB (Author: Lukas Meling)
 */

#ifndef UA_NETWORK_CoAP_H_
#define UA_NETWORK_CoAP_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "ua_plugin_pubsub.h"
/* CoAP network layer specific internal data */

UA_PubSubTransportLayer
UA_PubSubTransportLayerCoAPUDPMP(void);


#ifdef __cplusplus
} // extern "C"
#endif

#endif /* UA_NETWORK_CoAP_H_ */
