

#include "ua_network_pubsub_coap.h"

UA_StatusCode Discover(UA_PubSubChannelDataCoAP*);

UA_StatusCode Create(UA_PubSubChannelDataCoAP*);

UA_StatusCode Delete(UA_PubSubChannelDataCoAP*);

UA_StatusCode UnSubscribe(UA_PubSubChannelDataCoAP*);

UA_StatusCode Publish(UA_PubSubChannelDataCoAP*, const UA_ByteString *buffer);

UA_StatusCode Subscribe(UA_PubSubChannelDataCoAP*);

UA_StatusCode get_updates(UA_PubSubChannelDataCoAP*);

UA_StatusCode Read(UA_PubSubChannelDataCoAP*, UA_String topic);


   
