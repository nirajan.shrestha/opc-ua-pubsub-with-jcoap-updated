
 /* Assume that Windows versions are newer than Windows XP */
#if defined(__MINGW32__) && (!defined(WINVER) || WINVER < 0x501)
# undef WINVER
# undef _WIN32_WINDOWS
# undef _WIN32_WINNT
# define WINVER 0x0501
# define _WIN32_WINDOWS 0x0501
# define _WIN32_WINNT 0x0501
#endif

#ifdef _WIN32
# include <winsock2.h>
# include <ws2tcpip.h>
# include <Iphlpapi.h>
# define CLOSESOCKET(S) closesocket((SOCKET)S)
# define ssize_t int
# define UA_fd_set(fd, fds) FD_SET((unsigned int)fd, fds)
# define UA_fd_isset(fd, fds) FD_ISSET((unsigned int)fd, fds)
#else /* _WIN32 */
#  define CLOSESOCKET(S) close(S)
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <net/if.h>
# endif /* Not Windows */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "/usr/local/include/coap2/coap.h"
#include "/usr/local/include/coap2/net.h"
#include "/usr/local/include/coap2/coap_session.h"
#include "ua_coap_adapter.h"
#include "ua_plugin_network.h"
#include "ua_log_stdout.h"


UA_PubSubChannelDataCoAP * channelData;
coap_context_t*   ctx;
coap_context_t* context;
coap_address_t    dst_addr, src_addr;
coap_session_t *session;
static coap_uri_t uri;
fd_set            readfds, rfds; 
unsigned wait_ms = COAP_RESOURCE_CHECK_TIME * 1000;
int coap_fd, result, res, cfd; 
coap_pdu_t*       request;
const char*       server_uri = NULL;
const char* default_address = "coap://127.0.0.1/";

UA_NetworkAddressUrlDataType address;

/** prepares coap request message **/
static void prepare_request(){
	
	coap_address_init(&src_addr);
	src_addr.addr.sin.sin_family      = AF_INET;
	src_addr.addr.sin.sin_port        = htons(0);
	src_addr.addr.sin.sin_addr.s_addr = INADDR_ANY;
	ctx = coap_new_context(&src_addr);
	coap_fd = coap_context_get_coap_fd(ctx); 

	/* The destination endpoint */
	coap_address_init(&dst_addr);
	dst_addr.addr.sin.sin_family      = AF_INET;
	dst_addr.addr.sin.sin_port        = htons(5683);
	dst_addr.addr.sin.sin_addr.s_addr = inet_addr("127.0.0.1");

	/* Prepare the request */
	coap_split_uri(server_uri, strlen(server_uri), &uri);
	session = coap_new_client_session(ctx, &src_addr, &dst_addr, COAP_PROTO_UDP);
	request            = coap_new_pdu(session); 
	request->type = COAP_MESSAGE_CON;
	//request->type = COAP_MESSAGE_NON;
	request->tid   = coap_new_message_id(session);
}

/* Is called when a response message is received */
static void
message_handler(struct coap_context_t *ctx, coap_session_t *session, 
                coap_pdu_t *sent, coap_pdu_t *received, 
                const coap_tid_t id) 
{
	unsigned char* data;
	size_t         data_len;
	UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "handler called");
	if (COAP_RESPONSE_CLASS(received->code) == 2) {
		/* gets response from the broker */
		if (coap_get_data(received, &data_len, &data))
		{
			//printf("Received JSON Message : %s\n", data);
		}
	}
	
	if(channelData != NULL){
		if(channelData->callback != NULL){       
			
			/* callback with message and topic as bytestring. */
			UA_ByteString *msg = UA_ByteString_new(); 			
			UA_StatusCode ret = UA_ByteString_allocBuffer(msg, data_len);		
			if(ret){
				free(msg); 
				return;
			}
			memcpy(msg->data, data, data_len);
			/* callback message to subscriber */
			channelData->callback(msg);
		}
	} 
	 
}

/* keeps on listening to the socket for messages from the broker */
static void socket_read(){
	
	FD_ZERO(&readfds);
	FD_SET( coap_fd, &readfds );
	struct timeval tv;
	tv.tv_sec = wait_ms/1000;
	tv.tv_usec = (wait_ms%1000)*1000;
	result = select( coap_fd + 1, &readfds, 0, 0, &tv );
	
	if ( result < 0 ) /* socket error */
	{
		exit(EXIT_FAILURE);
	} 
	else if ( result > 0 && FD_ISSET(coap_fd, &readfds )) /* socket read*/
	{	
		coap_run_once( ctx, wait_ms );      
	} 
}

UA_StatusCode
Create(UA_PubSubChannelDataCoAP *channel) {

    	channelData =  channel;
     
	
	server_uri = "coap://127.0.0.1/ps"; 

	const char*	post_data; // payload to send for post request

	const char str1[] = "<>;ct=50";

	post_data = malloc(strlen(str1) + strlen((const char*)channelData->coapClientId->data));

	strcpy(post_data, "<");	
	strcat(post_data, (const char*)channelData->coapClientId->data);
	strcat(post_data, ">;ct=50");

	prepare_request();

	request->code = COAP_REQUEST_POST; // enable this for post request //
	coap_add_option(request, COAP_OPTION_URI_PATH, uri.path.length, uri.path.s);
	coap_add_data(request, strlen(post_data), (unsigned char *)post_data);

	/* Set the handler and send the request */
	coap_register_response_handler(ctx, message_handler);	
	coap_send(session, request);
	
	socket_read();

    return UA_STATUSCODE_GOOD;
}

UA_StatusCode
Read(UA_PubSubChannelDataCoAP *channel, UA_String topic) {

    channelData =  channel;
	
	server_uri = malloc(strlen(default_address) + strlen(topic.data)); //
	strcpy(server_uri, default_address);

	strcat(server_uri, (const char*)topic.data);

	prepare_request();

	request->code = COAP_REQUEST_GET; // enable this for get request //
	coap_add_option(request, COAP_OPTION_URI_PATH, uri.path.length, uri.path.s);

	/* Set the handler and send the request */
	coap_register_response_handler(ctx, message_handler);
	coap_send(session, request);
	
	socket_read();

    return UA_STATUSCODE_GOOD;
}

UA_StatusCode
Subscribe(UA_PubSubChannelDataCoAP *channel) {
UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "Subscribe method called");
    channelData =  channel;     
	
	default_address = "coap://127.0.0.1/ps/";

	unsigned char buf[3];
	static unsigned int token = 2;
	
	server_uri = malloc(strlen(default_address) + strlen(channelData->topic.data));
	strcpy(server_uri, default_address);
	strcat(server_uri, (const char*)channelData->topic.data);

	prepare_request();

	request->code = COAP_REQUEST_GET; 
	coap_add_token(request, sizeof(token), (unsigned char*)&token);
	coap_add_option(request, COAP_OPTION_OBSERVE, COAP_OBSERVE_ESTABLISH, NULL);// to subscribe to a resource
	coap_add_option(request, COAP_OPTION_URI_PATH, uri.path.length, uri.path.s);
	coap_add_option(request, COAP_OPTION_CONTENT_TYPE, coap_encode_var_safe(buf, sizeof(buf), COAP_MEDIATYPE_APPLICATION_JSON), buf);
	//coap_add_option(request, COAP_OPTION_CONTENT_TYPE, coap_encode_var_safe(buf, sizeof(buf), COAP_MEDIATYPE_APPLICATION_CBOR), buf);
	/* Set the handler and send the request */
	coap_register_response_handler(ctx, message_handler);	
	coap_send(session, request);
	
	socket_read();
	UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "Subscribed");

    return UA_STATUSCODE_GOOD;
}

UA_StatusCode
Publish(UA_PubSubChannelDataCoAP *channel, const UA_ByteString *buffer) {
    channelData =  channel;     
	
	default_address = "coap://127.0.0.1/ps/";
	
	unsigned char buf[3];
    server_uri = malloc(strlen(default_address) + strlen((const char*)channelData->topic.data));
	strcpy(server_uri, default_address);
	strcat(server_uri, (const char*)channelData->topic.data); 
	const char*	post_data = (*buffer).data; // payload to send for put request
	
	prepare_request();

	request->code = COAP_REQUEST_PUT; // for put request
	coap_add_option(request, COAP_OPTION_URI_PATH, uri.path.length, uri.path.s);
	coap_add_option(request, COAP_OPTION_CONTENT_TYPE, coap_encode_var_safe(buf, sizeof(buf), COAP_MEDIATYPE_APPLICATION_JSON), buf);
	//coap_add_option(request, COAP_OPTION_CONTENT_TYPE, coap_encode_var_safe(buf, sizeof(buf), COAP_MEDIATYPE_APPLICATION_CBOR), buf);
	coap_add_data(request, strlen(post_data), (unsigned char *)post_data); // for put request

	/* Set the handler and send the request */
	coap_register_response_handler(ctx, message_handler);
	coap_send(session, request);
	
	socket_read();
	UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "Published");

    return UA_STATUSCODE_GOOD;
}

UA_StatusCode
UnSubscribe(UA_PubSubChannelDataCoAP *channel) {

    channelData =  channel;     
	
	default_address = "coap://127.0.0.1/ps/";
	
	unsigned char buf[3];
	static unsigned int token = 2;
	
	server_uri = malloc(strlen(default_address) + strlen(channelData->topic.data));
	strcpy(server_uri, default_address);
	strcat(server_uri, (const char*)channelData->topic.data);

	prepare_request();

	request->code = COAP_REQUEST_GET; 
	coap_add_token(request, sizeof(token), (unsigned char*)&token);
	coap_add_option(request, COAP_OPTION_OBSERVE, coap_encode_var_safe(buf, sizeof(buf), COAP_OBSERVE_CANCEL), buf); // to unsubscribe
	coap_add_option(request, COAP_OPTION_URI_PATH, uri.path.length, uri.path.s);
	coap_add_option(request, COAP_OPTION_CONTENT_TYPE, coap_encode_var_safe(buf, sizeof(buf), COAP_MEDIATYPE_APPLICATION_JSON), buf);

	/* Set the handler and send the request */
	coap_register_response_handler(ctx, message_handler);	
	coap_send(session, request);
	
	socket_read();
	UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "Subscribed");

    return UA_STATUSCODE_GOOD;
}

UA_StatusCode
Delete(UA_PubSubChannelDataCoAP *channel) {

    channelData =  channel;     
	
	UA_String topic = channelData->topic;
	default_address = "coap://127.0.0.1/ps/";
	
	unsigned char buf[3];
    server_uri = malloc(strlen(default_address) + strlen(channelData->topic.data));
	strcpy(server_uri, default_address);
	strcat(server_uri, channelData->topic.data);
	
	prepare_request();

	request->code = COAP_REQUEST_DELETE; 
	coap_add_option(request, COAP_OPTION_URI_PATH, uri.path.length, uri.path.s);
	coap_add_option(request, COAP_OPTION_CONTENT_TYPE, coap_encode_var_safe(buf, sizeof(buf), COAP_MEDIATYPE_APPLICATION_JSON), buf);

	/* Set the handler and send the request */
	coap_register_response_handler(ctx, message_handler);
	coap_send(session, request);
	
	socket_read();
	UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "Deleted");

    return UA_STATUSCODE_GOOD;
}

#undef _POSIX_C_SOURCE
