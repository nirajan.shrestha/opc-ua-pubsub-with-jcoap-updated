
 /* Assume that Windows versions are newer than Windows XP */
#if defined(__MINGW32__) && (!defined(WINVER) || WINVER < 0x501)
# undef WINVER
# undef _WIN32_WINDOWS
# undef _WIN32_WINNT
# define WINVER 0x0501
# define _WIN32_WINDOWS 0x0501
# define _WIN32_WINNT 0x0501
#endif

#ifdef _WIN32
# include <winsock2.h>
# include <ws2tcpip.h>
# include <Iphlpapi.h>
# define CLOSESOCKET(S) closesocket((SOCKET)S)
# define ssize_t int
# define UA_fd_set(fd, fds) FD_SET((unsigned int)fd, fds)
# define UA_fd_isset(fd, fds) FD_ISSET((unsigned int)fd, fds)
#else /* _WIN32 */
#  define CLOSESOCKET(S) close(S)
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <net/if.h>
# endif /* Not Windows */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "/usr/local/include/coap2/coap.h"
#include "/usr/local/include/coap2/net.h"
#include "/usr/local/include/coap2/coap_session.h"

#include "ua_plugin_network.h"
#include "ua_network_pubsub_coap.h"
#include "CoAP/ua_coap_adapter.h"
#include "CoAP/ua_coap_adapter.c"
#include "ua_log_stdout.h"
#include <ua_network_tcp.h>
#include <ua_util.h>


UA_PubSubChannelDataCoAP * channelData;
UA_NetworkAddressUrlDataType address;


static UA_PubSubChannel *
UA_PubSubChannelCoAP_Open(const UA_PubSubConnectionConfig *connectionConfig) {  

    if(UA_Variant_hasScalarType(&connectionConfig->address, &UA_TYPES[UA_TYPES_NETWORKADDRESSURLDATATYPE])){
        address = *(UA_NetworkAddressUrlDataType *)connectionConfig->address.data;
    } else {
        UA_LOG_ERROR(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "PubSub Connection creation failed. Invalid Address.");
        return NULL;
    }	

	//printf("UA_NetworkAddressUrlDataType:  %s\n",address.url.data);	

   //allocate and init memory for the CoAP specific internal data
    channelData = (UA_PubSubChannelDataCoAP *) UA_calloc(1, (sizeof(UA_PubSubChannelDataCoAP)));
    if(!channelData){
        UA_LOG_ERROR(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "PubSub Connection creation failed. Out of memory.");
        return NULL;
    }
	
	 //set default values
    UA_String topic = UA_STRING("coapClientId");
    memcpy(channelData, &(UA_PubSubChannelDataCoAP){&topic, &topic, NULL}, sizeof(UA_PubSubChannelDataCoAP));
	
    UA_String hostname, path;
    UA_UInt16 networkPort;
    if(UA_parseEndpointUrl(&address.url, &hostname, &networkPort, &path) != UA_STATUSCODE_GOOD){
        UA_LOG_ERROR(UA_Log_Stdout, UA_LOGCATEGORY_SERVER,
                     "PubSub Connection creation failed. Invalid URL.");
        return UA_STATUSCODE_BADINVALIDARGUMENT;
    } 
	
    for(size_t i = 0; i < connectionConfig->connectionPropertiesSize; i++){
       if(UA_String_equal(&connectionConfig->connectionProperties[i].key.name, &topic)){
            if(UA_Variant_hasScalarType(&connectionConfig->connectionProperties[i].value, &UA_TYPES[UA_TYPES_STRING])){
                channelData->coapClientId = (UA_String *) connectionConfig->connectionProperties[i].value.data;
				Create(channelData); // Create a topic
            }
        } else {
            UA_LOG_WARNING(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "PubSub Connection creation. Unknown connection parameter.");
        }
    }
	
    /* Create a new channel */
    UA_PubSubChannel *newChannel = (UA_PubSubChannel *) UA_calloc(1, sizeof(UA_PubSubChannel));
    if(!newChannel){
        UA_LOG_ERROR(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "PubSub Connection creation failed. Out of memory.");
        return NULL;
    }
    
    UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "CoAP Connection established.");
    
    newChannel->state = UA_PUBSUB_CHANNEL_RDY;
	
    return newChannel;
}

static void UA_PubSubChannelCoAP_get_updates(UA_PubSubChannel *channel){

	socket_read(); // keeps on listening on a socket for new updates on a topic
	
}

static void
UA_PubSubChannelCoAP_Get(UA_PubSubChannel *channel, UA_ByteString *topic, UA_ExtensionObject *transportSettings, UA_UInt32 timeout) {
   
	UA_BrokerWriterGroupTransportDataType *brokerTransportSettings = (UA_BrokerWriterGroupTransportDataType*)transportSettings->content.decoded.data;

    channelData = (UA_PubSubChannelDataCoAP *) channel->handle;
	
    Read(channelData, brokerTransportSettings->queueName);
	
	UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "GET Request Sent");
    
}

static void
UA_PubSubChannelCoAP_Subscribe(UA_PubSubChannel *channel, UA_ExtensionObject *transportSettigns, void (*callback)()) {
    if(!(channel->state == UA_PUBSUB_CHANNEL_PUB_SUB || channel->state == UA_PUBSUB_CHANNEL_RDY)){
        UA_LOG_ERROR(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "PubSub Connection subscribe failed.");
        return UA_STATUSCODE_BADINTERNALERROR;
    }
    UA_StatusCode ret = UA_STATUSCODE_GOOD;
    
    channelData = (UA_PubSubChannelDataCoAP *) channel->handle;
    channelData->callback = callback;
    
     if(transportSettigns != NULL && transportSettigns->encoding == UA_EXTENSIONOBJECT_DECODED 
            && transportSettigns->content.decoded.type->typeIndex == UA_TYPES_BROKERWRITERGROUPTRANSPORTDATATYPE){
        UA_BrokerWriterGroupTransportDataType *brokerTransportSettings = (UA_BrokerWriterGroupTransportDataType*)transportSettigns->content.decoded.data;
	
	channelData->topic = brokerTransportSettings->queueName;	
	
	Subscribe(channelData);
	
    }
    
}

static UA_StatusCode
UA_PubSubChannelCoAP_Publish(UA_PubSubChannel *channel, UA_ExtensionObject *transportSettings, const UA_ByteString *buffer) {

    channelData = (UA_PubSubChannelDataCoAP *) channel->handle;

    if(!(channel->state == UA_PUBSUB_CHANNEL_PUB || channel->state == UA_PUBSUB_CHANNEL_PUB_SUB)){
        UA_LOG_WARNING(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "PubSub Connection sending failed. Invalid state.");
        return UA_STATUSCODE_BADINTERNALERROR;
    }
    
    UA_StatusCode ret = UA_STATUSCODE_GOOD;
    
    if(transportSettings != NULL && transportSettings->encoding == UA_EXTENSIONOBJECT_DECODED 
            && transportSettings->content.decoded.type->typeIndex == UA_TYPES_BROKERWRITERGROUPTRANSPORTDATATYPE){
        UA_BrokerWriterGroupTransportDataType *brokerTransportSettings = (UA_BrokerWriterGroupTransportDataType*)transportSettings->content.decoded.data;
        
		channelData->topic = brokerTransportSettings->queueName;	
		
		Publish(channelData, buffer);
	}
	//channelData->topic = UA_STRING("hello");	
		
	//Publish(channelData, buffer);
    return ret;
}

static void
UA_PubSubChannelCoAP_UnSubscribe(UA_PubSubChannel *channel, UA_ExtensionObject *transportSettigns) {
	
    if(!(channel->state == UA_PUBSUB_CHANNEL_PUB_SUB || channel->state == UA_PUBSUB_CHANNEL_RDY)){
        UA_LOG_ERROR(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "PubSub Connection subscribe failed.");
        return UA_STATUSCODE_BADINTERNALERROR;
    }
    UA_StatusCode ret = UA_STATUSCODE_GOOD;
    
    channelData = (UA_PubSubChannelDataCoAP *) channel->handle;
    
     if(transportSettigns != NULL && transportSettigns->encoding == UA_EXTENSIONOBJECT_DECODED 
            && transportSettigns->content.decoded.type->typeIndex == UA_TYPES_BROKERWRITERGROUPTRANSPORTDATATYPE){
        UA_BrokerWriterGroupTransportDataType *brokerTransportSettings = (UA_BrokerWriterGroupTransportDataType*)transportSettigns->content.decoded.data;
	
	channelData->topic = brokerTransportSettings->queueName;	
	
	UnSubscribe(channelData);	
           
    }
    
}

static UA_StatusCode
UA_PubSubChannelCoAP_Delete(UA_PubSubChannel *channel) {
  
    channelData = (UA_PubSubChannelDataCoAP *) channel->handle;
	
	 if(!(channel->state == UA_PUBSUB_CHANNEL_PUB || channel->state == UA_PUBSUB_CHANNEL_PUB_SUB)){
        UA_LOG_WARNING(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "PubSub Connection sending failed. Invalid state.");
        return UA_STATUSCODE_BADINTERNALERROR;
    }
	
	Delete(channelData);        
    
    return UA_STATUSCODE_GOOD;
}

/**
 * Generate a new channel. based on the given configuration.
 *
 * @param connectionConfig connection configuration
 * @return  ref to created channel, NULL on error
 */
// this method is invoked after tl->createPubSubChannel(newConnection->config) within UA_Server_addPubSubConnection()
static UA_PubSubChannel *
TransportLayerCoAP_addChannel(UA_PubSubConnectionConfig *connectionConfig) {
    UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "PubSub channel requested");
    UA_PubSubChannel * pubSubChannel = UA_PubSubChannelCoAP_Open(connectionConfig);
	if(pubSubChannel){
		pubSubChannel->regist = UA_PubSubChannelCoAP_Subscribe;
		pubSubChannel->unregist = UA_PubSubChannelCoAP_UnSubscribe;
		pubSubChannel->send = UA_PubSubChannelCoAP_Publish;
		pubSubChannel->receive = UA_PubSubChannelCoAP_Get;
		pubSubChannel->close = UA_PubSubChannelCoAP_Delete;
		pubSubChannel->yield = UA_PubSubChannelCoAP_get_updates;
	}
 	pubSubChannel->connectionConfig = connectionConfig;
    return pubSubChannel;
}

//CoAP channel factory
UA_PubSubTransportLayer
UA_PubSubTransportLayerCoAP(){
    UA_PubSubTransportLayer pubSubTransportLayer;
    pubSubTransportLayer.transportProfileUri = UA_STRING("http://opcfoundation.org/UA-Profile/Transport/pubsub-CoAP");  // transportProfileUri for CoAP
    pubSubTransportLayer.createPubSubChannel = &TransportLayerCoAP_addChannel;
    return pubSubTransportLayer;
}

#undef _POSIX_C_SOURCE
