/* This work is licensed under a Creative Commons CCZero 1.0 Universal License.
 * See http://creativecommons.org/publicdomain/zero/1.0/ for more information.
 * 
 *    Copyright 2018 (c) Fraunhofer IOSB (Author: Lukas Meling)
 */

#ifndef UA_NETWORK_CoAP_H_
#define UA_NETWORK_CoAP_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "ua_plugin_pubsub.h"
/* CoAP network layer specific internal data */
typedef struct {
    UA_String topic;
    UA_String *coapClientId;
    void (*callback)(UA_ByteString *encodedBuffer);

} UA_PubSubChannelDataCoAP;

UA_PubSubTransportLayer
UA_PubSubTransportLayerCoAP(void);


#ifdef __cplusplus
} // extern "C"
#endif

#endif /* UA_NETWORK_CoAP_H_ */
